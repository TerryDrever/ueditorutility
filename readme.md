# Overview

This repository will hold classes that should help to improve ease of development with Unity Tools.

### Solving A Common Problem
When writing Auto Layout'd gui extensions with Unity, it's always important to remember that almost every action has to have and opposite.

#### Example
Take GUILayout.BeginVertical, for example :

```c
	GUILayout.BeginVertical();
	{
		// Do something here
	}
	GUILayout.EndVertical();
```

This is all well and good, functionality wise, but the user must remember to EndVertical

Instead, with these Utility class, you can do the following

```c
	using(new GUIBeginVertical())
	{
		// Do something here
	}
```

#### Another Example

```c
	var previousColour = GUI.Color;
	GUI.color = Color.red;
	{
		// Do something with red GUI
	}
	GUI.color = previousColour;
```

becomes

```c
	using(new GUIChangeColor(Color.red))
	{
		// Do something with red GUI
	}
```

With this new approach, the programmer doesn't need to remember to EndVertical, the code takes care of that for them. This isn't exactly revolutionary stuff, and is a commonly used programming idiom, if you'd like to know more, research RAII.