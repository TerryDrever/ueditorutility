﻿using UnityEngine;
using System;
using System.Collections;

namespace WellFired
{
	public class GUIBeginHorizontal : IDisposable
	{
		public GUIBeginHorizontal()
		{
			GUILayout.BeginHorizontal();
		}
		
		public GUIBeginHorizontal(params GUILayoutOption[] layoutOptions)
		{
			GUILayout.BeginHorizontal(layoutOptions);
		}
		
		public void Dispose() 
		{
			GUILayout.EndHorizontal();
		}
	}
}