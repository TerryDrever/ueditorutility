﻿using UnityEngine;
using System;
using System.Collections;

namespace WellFired
{
	public class GUIBeginVertical : IDisposable
	{
		public GUIBeginVertical()
		{
			GUILayout.BeginVertical();
		}
	
		public GUIBeginVertical(params GUILayoutOption[] layoutOptions)
		{
			GUILayout.BeginVertical(layoutOptions);
		}
		
		public void Dispose() 
		{
			GUILayout.EndVertical();
		}
	}
}