﻿using UnityEngine;
using System;
using System.Collections;

namespace WellFired
{
	public class GUIBeginScrollView : IDisposable
	{
		public Vector2 Scroll
		{
			get;
			set;
		}
	
		public GUIBeginScrollView(Vector2 scrollPosition)
		{
			Scroll = GUILayout.BeginScrollView(scrollPosition);
		}
		
		public GUIBeginScrollView(Vector2 scrollPosition, GUIStyle horizontalScrollBar, GUIStyle verticalScrollBar)
		{
			Scroll = GUILayout.BeginScrollView(scrollPosition, horizontalScrollBar, verticalScrollBar);
		}
		
		public void Dispose() 
		{
			GUILayout.EndScrollView();
		}
	}
}